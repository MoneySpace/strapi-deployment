#!/bin/bash
git pull

git submodule update --recursive

echo Initializing docker network money-space-proxy

if [[ -n $(docker network ls | grep money-space-proxy) ]]
then
    echo Already exists, skipping
else
    echo Network not found, creating
    docker network create money-space-proxy
fi

echo Building and deploying nuxt repo
cd nuxt-typescript
docker-compose up -d --build
cd ../

echo Building and deploying strapi repo
cd strapi
docker-compose up -d --build
cd ../

echo Starting nginx
cd nginx
docker-compose up -d --build
cd ../

echo DONE

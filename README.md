# MONEYSPACE Deployment repo
---
Clone this repo recursively
`git clone --recursive git@bitbucket.org:MoneySpace/strapi-deployment.git`
This will checkout deployment repo itself along with 2 other repositories including `strapi (backend)` and `nuxtjs (frontend)`

## Start eveything

To start all services, simply run `./install.sh`. 
> If you see error `permission denied: ./install.sh`, run `chmod +x install.sh` first.

## `install.sh` explained

1. It ensures the Docker network named `money-space-proxy` which links `nginx`, `strapi`, and `nuxtjs` services together. Specifically for `nginx` to know the other two services.
2. It builds and starts `strapi`, `nuxtjs` services accordingly.

## Debugging Tips

### changing ip or domain

Replace all occurrences of current ip (`172.104.207.207`) to the new domain. Make sure "every" string are replaced. Known files are as follows:

### nothing from strapi

Ensure that nginx is configured properly. Check nginx/conf.d/default.conf and all `docker-compose.yml` files. `container_name` should match `server` section in the conf file. More doc on nginx deployment, please consult https://strapi.io/documentation/developer-docs/latest/deployment/nginx-proxy.html

### strapi failed to build, `JavaScript heap out of memory`

Make sure your server has decent amount of memory and space. Swap may help if you can't increase memory but has some space left (5-10GB). To make swap, follow these steps https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-16-04


Edit `Dockerfile` by seting memory limit at line  `NODE_OPTIONS=--max_old_space_size=<YOUR_MEMORY_SIZE>`. Default is set to `3072` (3GB) which should be suffice for most machines.